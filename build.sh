#!/bin/bash

#Usage ./build.sh <registry address> <container namespace/path>
#Example: ./build.sh registry.someurl.com:4567 path/to/orgs/container 
set -e

if [ -z ${1+x} ]; 
  then echo "Registry address has not been specified";
  exit 1
else 
  registry_address=$1
fi

if [ -z ${2+x} ];
  then echo "Container namespace/path has not been specified";
  exit 1
else
  namespace="$2"
fi

image_name="$registry_address/$namespace/centos-ssh"

# cd to where build script is located
# allows easy predictable paths everywhere in build chain
cd "$( dirname "${BASH_SOURCE[0]}" )"

echo "Building and tagging image as $image_name"
docker build --pull -t $image_name .
