FROM centos:7 

LABEL vendor="US DOT, FAA, Office of NextGen, Modeling and Simulation Branch" \
      version="0.2.1" \
      description="Docker container to support SSH in CentOS"

RUN yum -y update && \
    yum install -y openssh-clients sshpass && \
    yum clean all

RUN mkdir -p ~/.ssh && \
    chmod 700 ~/.ssh

