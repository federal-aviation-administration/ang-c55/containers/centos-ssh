#!/bin/bash

#Usage ./push.sh <registry address> <container namespace/path>
#Example: ./push.sh registry.someurl.com:4567 path/to/orgs/container
set -e

if [ -z ${1+x} ];
  then echo "Registry address has not been specified";
  exit 1
else
  registry_address=$1
fi

if [ -z ${2+x} ];
  then echo "Container namespace/path has not been specified";
  exit 1
else
  namespace="$2"
fi

image_name="$registry_address/$namespace/centos-ssh"

# cd to where build script is located
# allows easy predictable paths everywhere in build chain
cd "$( dirname "${BASH_SOURCE[0]}" )"

#Use the version from the container label for the tag in the registry
version_tag=$(docker inspect --format='{{.ContainerConfig.Labels.version}}' $image_name)

#check to see if a version tag is set before pushing the image to the registry
if [ "$version" = "<no value>" ]; then
	echo "No version label present at '.ContainerConfig.Labels.version'! Ensure version is set via LABEL before pushing"; 
	exit 1;
fi

docker tag  $image_name $image_name:$version_tag
docker push $image_name:latest
docker push $image_name:$version_tag
